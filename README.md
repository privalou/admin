# Бот администратор самарского айти сообщества
![Rust CI](https://github.com/SamaraITCommunity/admin/workflows/Rust%20CI/badge.svg)

## Функционал

### Размещение постов из [VK](https://vk.com/samara_it_community):

✔️ Push в [репозиторий сайта sitc.community](https://github.com/SamaraITCommunity/site).

❌ Публикация в twitter. //TODO: Сообществу не одобрили аккаунт разработчика - возможно стоит использовать IFTTT.

✔️ Публикация в [telegram-канале](t.me/Samara_IT_Community).

✔️ Публикация в [Discord](https://discord.gg/Vtnrgym).

✔️ Упоминания сообществ и людей (‘@’ ‘*’).

✔️ Очищать теги от `@samara_it_community`.

⚠️ Поддержка подкастов (ограниченная поддержка).

⚠️ Поддержка статей и их превью (ограниченная поддержка).

✔️ Для хранения изображений используется LFS или внешний сервис (все изображения хранятся только на серверах ВК).


## Ввод в эксплуатацию

1. Создать и заполнить `.env` файл.
1. `./admin`

### Переменные окружения

#### Vkontakte

`VK_GROUP_ID`

ID группы VK. Может быть как отрицательным, так и положительным.

`VK_API_KEY`

Токен для обращения к API ВК. Пользовательский токен можно получить здесь: https://vkhost.github.io/. Доступность методов можно посмотреть [тут](https://vk.com/dev/methods) и [тут](https://vk.com/dev/permissions)

#### Telegram

`TELEGRAM_API_KEY`

Token Telegram бота. Можно получить у @BotFather.

`TELEGRAM_CHANNEL_ID`

Ссылка на канал без `https://tg.me/` части или ID (с @ или без).

#### Discord

`DISCORD_API_KEY`

Токен Discord-бота. Можно получить [тут](https://discordapp.com/developers/applications/).
Приглашать в гильдию по ссылке: `https://discordapp.com/oauth2/authorize?client_id={BOT_ID}&scope=bot&permissions=452672`

`DISCORD_CHANNEL_ID`

Включить режим разработчика в Discord, правая кнопка мыши на канале, Copy ID.

#### Gitlab

`GITLAB_BRANCH`

Target branch to commit news to. `test.env` uses non-deployable branch,
while `.env` should target deployable branch.

`GITLAB_PERSONAL_TOKEN`

[Personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). 

`GITLAB_PROJECT_ID`

Your's project identification. Can be found in Project->Settings->General
