use super::Post;
use discord::model::ChannelId;
use discord::Discord as Origin;

pub struct Discord {
    origin: Origin,
    channel_id: u64,
}

impl Discord {
    pub fn new(token: String, channel_id: u64) -> Result<Self, String> {
        Ok(Discord {
            origin: Origin::from_bot_token(&token)
                .map_err(|e| format!("Failed to initialize discord: {}", e))?,
            channel_id,
        })
    }

    pub fn publish(&self, post: &Post) -> Result<(), String> {
        self.origin
            .send_message(ChannelId(self.channel_id), &post.text, "", false)
            .map_err(|e| format!("Failed to send message to Discord: {}", e))?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::env;

    #[test]
    fn publish_post() {
        dotenv::from_filename("test.env").expect("Failed to read env variables from test.env");
        let discord = Discord::new(
            env::var("DISCORD_API_KEY").expect("Set DISCORD_API_KEY environment variable"),
            env::var("DISCORD_CHANNEL_ID")
                .expect("Set DISCORD_CHANNEL_ID environment variable")
                .parse()
                .expect("DISCORD_CHANNEL_ID should be integer"),
        )
        .expect("Failed to build discord");
        let post = Post::new(1, String::from("Title\nDescription"));
        discord
            .publish(&post)
            .expect("Failed to publish Discord post");
    }
}
