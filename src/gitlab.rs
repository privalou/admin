use super::Post;
use gitlab;

pub struct Gitlab {
    origin: gitlab::Gitlab,
    project_id: gitlab::ProjectId,
    target_branch: String,
}

impl Gitlab {
    pub fn new(token: String, project_id: u64, target_branch: String) -> Result<Self, String> {
        Ok(Gitlab {
            origin: gitlab::Gitlab::new("gitlab.com", token)
                .map_err(|e| format!("Failed to create Gitlab client: {}", e))?,
            project_id: gitlab::ProjectId::new(project_id),
            target_branch,
        })
    }

    pub fn publish(&self, post: &Post) -> Result<(), String> {
        let create_file = CreateFile::from(post);
        self.origin
            .create_file(
                self.project_id,
                create_file.path,
                &self.target_branch,
                create_file.content,
                create_file.message,
            )
            .map_err(|e| format!("{}", e))?;
        Ok(())
    }
}

struct CreateFile {
    path: String,
    message: String,
    content: String,
}

impl From<&Post> for CreateFile {
    fn from(post: &Post) -> CreateFile {
        let content = format!(
            "+++\n title = \"{}\"\n date = {}\n description = \"{}\"\n+++\n\n{}",
            post.title,
            post.date_time.get(0..10).unwrap(),
            post.text.lines().next().unwrap_or("..."),
            post.text,
            //TODO: add attachments
        );
        CreateFile {
            path: urlencoding::encode(&format!("content/{}.md", post.date_time,)),
            message: format!("Новый пост - {}.md", post.title),
            content,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use dotenv;
    use std::env;

    #[test]
    fn github_new() {
        dotenv::from_filename("test.env").expect("Failed to read env variables from test.env");
        Gitlab::new(
            env::var("GITLAB_PERSONAL_TOKEN")
                .expect("Please set GITLAB_PERSONAL_TOKEN env variable"),
            env::var("GITLAB_PROJECT_ID")
                .expect("Please set GITLAB_PROJECT_ID env variable")
                .parse()
                .expect("Failed to parse GITLAB_PROJECT_ID"),
            env::var("GITLAB_BRANCH").expect("Please set GITLAB_BRANCH env variable"),
        )
        .expect("Failed to build Gitlab");
    }

    #[test]
    fn create_file_from_post() {
        let post = Post::new(1, String::from("Title\nDescription"));
        let create_file = CreateFile::from(&post);
        assert_eq!("Title", post.title);
        assert_eq!("Новый пост - Title.md", create_file.message);
        assert!(create_file.content.contains("Description"));
        assert!(create_file.content.contains("Title"));
    }

    #[test]
    fn publish_post() {
        dotenv::from_filename("test.env").expect("Failed to read env variables from test.env");
        let gitlab = Gitlab::new(
            env::var("GITLAB_PERSONAL_TOKEN")
                .expect("Please set GITLAB_PERSONAL_TOKEN env variable"),
            env::var("GITLAB_PROJECT_ID")
                .expect("Please set GITLAB_PROJECT_ID env variable")
                .parse()
                .expect("Failed to parse GITLAB_PROJECT_ID"),
            env::var("GITLAB_BRANCH").expect("Please set GITLAB_BRANCH env variable"),
        )
        .expect("Failed to build Gitlab");
        let post = Post::new(1, String::from("Title\nDescription"));
        gitlab.publish(&post).expect("Failed to publish post");
    }
}
